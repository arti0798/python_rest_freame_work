# from _typeshed import Self
from django.db import models

class Course(models.Model):
    # def __init__(self: _Self, *args, **kwargs) -> None:
    #     super().__init__(*args, **kwargs)

    name = models.CharField(max_length=200) 
    language = models.CharField(max_length=100)
    price = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    # def __str__(self):
    #     return self.name